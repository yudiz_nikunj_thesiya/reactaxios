import { Routes, Route } from "react-router-dom";
import React from "react";
import AllPosts from "./pages/AllPosts";
import EditPost from "./pages/EditPost";
import AddPost from "./pages/AddPost";
import ViewPost from "./pages/ViewPost";

function App() {
	return (
		<div className="">
			<Routes>
				<Route path="/" element={<AllPosts />} />
				<Route path="/edit/:id" element={<EditPost />} />
				<Route path="/add" element={<AddPost />} />
				<Route path="/view/:id" element={<ViewPost />} />
			</Routes>
		</div>
	);
}

export default App;
