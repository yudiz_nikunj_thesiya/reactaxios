import React, { useState } from "react";
import { Link } from "react-router-dom";
import { MdClose } from "react-icons/md";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const AddPost = () => {
	const [post, setPost] = useState({
		title: "",
		body: "",
	});

	const navigate = useNavigate();

	const onInputChange = (e) => {
		setPost({ ...post, [e.target.name]: e.target.value });
	};

	const onSubmit = async (e) => {
		e.preventDefault();
		await axios.post("http://localhost:3002/data", post);
		// console.log(contact);
		navigate("/");
	};
	return (
		<div className="absolute z-10 top-0 left-0 w-full h-screen text-gray-700 bg-gray-300 backdrop-filter bg-opacity-50 backdrop-blur-xl flex items-center justify-center">
			<div className="bg-white w-4/5 flex flex-col px-10 py-8 rounded-2xl shadow-xl space-y-5">
				<div className="flex items-center justify-between">
					<h4 className="text-xl">Create New Blog</h4>
					<Link
						to="/"
						className="text-lg bg-gray-200 p-3 rounded-full cursor-pointer"
					>
						<MdClose />
					</Link>
				</div>
				<form className="flex flex-col space-y-3" onSubmit={(e) => onSubmit(e)}>
					<div className="flex flex-col space-y-6">
						<div className="flex flex-col space-y-3">
							<span>Title</span>
							<input
								type="text"
								placeholder="title"
								className="input"
								name="title"
								value={post.title}
								onChange={(e) => onInputChange(e)}
								required
							/>
						</div>
						<div className="flex flex-col space-y-3">
							<span>Description</span>
							<textarea
								placeholder="description"
								className="input"
								name="body"
								value={post.body}
								rows={5}
								onChange={(e) => onInputChange(e)}
								required
							></textarea>
						</div>
					</div>

					<div className="pt-4 flex items-center justify-end">
						<input
							type="submit"
							value="Create Blog"
							className="btn cursor-pointer"
						/>
					</div>
				</form>
			</div>
		</div>
	);
};

export default AddPost;
