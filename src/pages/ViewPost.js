import axios from "axios";
import React, { useEffect, useState } from "react";
import { MdClose } from "react-icons/md";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import Avatar from "avataaars";

const ViewPost = () => {
	const [post, setPost] = useState({
		title: "",
		body: "",
	});

	const navigate = useNavigate();
	const { id } = useParams();

	const BASE_URL = "http://localhost:3002/data";

	const loadPosts = async () => {
		const result = await axios.get(`${BASE_URL}/${id}`);
		setPost(result.data);
		// console.log(result.data);
	};

	const onInputChange = (e) => {
		setPost({ ...post, [e.target.name]: e.target.value });
	};

	useEffect(() => {
		loadPosts();
	}, []);

	const onSubmit = async (e) => {
		e.preventDefault();
		await axios.put(`${BASE_URL}/${id}`, post);
		// console.log(post);
		navigate("/");
	};

	return (
		<div className="absolute z-10 top-0 left-0 w-full h-screen text-gray-700 bg-gray-300 backdrop-filter bg-opacity-50 backdrop-blur-xl flex items-center justify-center">
			<div className="bg-white w-4/5 flex flex-col px-10 py-8 rounded-2xl shadow-xl space-y-5">
				<div className="flex items-center justify-between">
					<Avatar
						avatarStyle="Circle"
						topType="LongHairMiaWallace"
						accessoriesType="Prescription02"
						hairColor="BrownDark"
						facialHairType="Blank"
						clotheType="Hoodie"
						clotheColor="PastelBlue"
						eyeType="Happy"
						eyebrowType="Default"
						mouthType="Smile"
						skinColor="Light"
						className="w-12 h-12"
					/>
					<Link
						to="/"
						className="text-lg bg-gray-200 p-3 rounded-full cursor-pointer"
					>
						<MdClose />
					</Link>
				</div>
				<div className="flex flex-col space-y-3">
					<div className="flex flex-col space-y-6">
						<div className="flex flex-col space-y-3">
							<span className="text-2xl font-bold">{post.title}</span>
						</div>
						<div className="flex flex-col space-y-3">
							<span className="text-lg">{post.body}</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default ViewPost;
