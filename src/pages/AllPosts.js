import React, { useState, useEffect } from "react";
import { MdDelete, MdModeEdit, MdClose } from "react-icons/md";
import Avatar from "avataaars";
import { Link } from "react-router-dom";
import axios from "axios";
import { FiEye } from "react-icons/fi";

const AllPosts = () => {
	const [posts, setPosts] = useState([]);
	const [home, setHome] = useState(true);

	const [post, setPost] = useState({
		title: "",
		body: "",
	});

	const BASE_URL = "http://localhost:3002/data";

	useEffect(() => {
		loadPosts();
	}, []);

	const loadPosts = async () => {
		const result = await axios.get(`${BASE_URL}`);
		setPosts(result.data);
	};

	const deleteContact = async (id) => {
		alert("Do you want to delete this contact?");
		await axios.delete(`http://localhost:3002/data/${id}`);
		loadPosts();
	};

	return (
		<div className="flex flex-col items-center">
			<div className="w-full px-6 py-8 md:py-12 md:px-10 flex flex-col">
				<div className="w-full text-right">
					<Link to="/add" className="btn">
						+ Create Blog
					</Link>
				</div>

				<h4 className="text-xl font-semibold text-gray-800 my-5">
					Total Blogs {posts.length}
				</h4>

				<div className="grid grid-cols-1 lg:grid-cols-2 gap-4">
					{posts.map((post, index) => (
						<div
							className="w-full blog-card bg-white rounded-xl px-6 py-4 md:px-8 md:py-6 flex flex-col md:flex-col space-y-2 md:space-y-0 md:items-start justify-between border border-transparent hover:border-blue-200"
							key={post.id}
						>
							<div className="flex w-full flex-col space-y-6">
								<div className="flex items-center justify-between w-full">
									<Avatar
										avatarStyle="Circle"
										topType="LongHairMiaWallace"
										accessoriesType="Prescription02"
										hairColor="BrownDark"
										facialHairType="Blank"
										clotheType="Hoodie"
										clotheColor="PastelBlue"
										eyeType="Happy"
										eyebrowType="Default"
										mouthType="Smile"
										skinColor="Light"
										className="w-12 h-12"
									/>
									<div className="card-btns flex space-x-3">
										<Link
											to={`view/${post.id}`}
											className="p-3 rounded-lg text-lg hover:bg-yellow-500 hover:text-white bg-yellow-200 text-yellow-500"
										>
											<FiEye />
										</Link>
										<Link
											to={`edit/${post.id}`}
											className="p-3 rounded-lg text-lg hover:bg-blue-500 hover:text-white bg-blue-200 text-blue-500"
										>
											<MdModeEdit />
										</Link>
										<span
											onClick={() => deleteContact(post.id)}
											className="p-3 cursor-pointer rounded-lg text-lg hover:bg-red-400 hover:text-white bg-red-200 text-red-500"
										>
											<MdDelete />
										</span>
									</div>
								</div>
								<div className="flex flex-col space-y-1">
									<h3 className="text-lg line-clamp-2 font-semibold text-gray-800">
										{post.title}
									</h3>
									<h5 className="text-gray-600 line-clamp-3 text-sm">
										{post.body}
									</h5>
								</div>
							</div>
						</div>
					))}
				</div>
			</div>
		</div>
	);
};

export default AllPosts;
